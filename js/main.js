;(function() {

	var tabs = document.querySelectorAll('.nav-menu .content-menu-item');
	var tabsItems = document.querySelectorAll('.content-list .content-list-item');
		
	for (var i = 0; i < tabs.length; i++){
		
		tabs[i].addEventListener('click', function(event) {
			event.preventDefault();

			for (var j = 0; j < tabs.length; j++){
				tabs[j].classList.remove('active');
				tabsItems[j].classList.remove('active');
			}

			var tabsItemId = this.firstElementChild.getAttribute('href');
			
			document.getElementById(tabsItemId).classList.add('active');
			this.classList.add('active');
		})
	}
})();


function welcomButtonClick(e){
	var welcomScreen = document.querySelector('.welcom');
	var footer = document.querySelector('.footer');
	var header = document.querySelector('.header');
	var content = document.querySelector('article.content');
	footer.style.opacity = '1';
	header.style.marginTop = '0px';
	content.style.height = 'auto';
	content.style.overflow = 'auto';
	content.style.opacity = 1;
	welcomScreen.setAttribute('hidden','');
}

document.querySelector('.welcom-link').addEventListener('click', welcomButtonClick);
