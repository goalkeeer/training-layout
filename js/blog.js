function openMenuClick(e){
	e.preventDefault();

	var openMenu = document.querySelector('.blog-menu');
	openMenu.classList.toggle('active');
}

document.querySelector('.button-openmenu').addEventListener('click', openMenuClick)